import { initializeApp } from 'firebase/app';
import { FacebookAuthProvider, getAuth, GoogleAuthProvider } from 'firebase/auth';

const firebaseConfig = {
    apiKey: 'AIzaSyBWuoJOjwprjft2JNEEOy7UB4iCuNa78gk',
    authDomain: 'webcuuho.firebaseapp.com',
    projectId: 'webcuuho',
    storageBucket: 'webcuuho.appspot.com',
    messagingSenderId: '543118459922',
    appId: '1:543118459922:web:65b193bd5cb231941984cb',
    measurementId: 'G-JDT5P4LC25'
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const provider = new GoogleAuthProvider();
const providerFb = new FacebookAuthProvider();
export { auth, provider, providerFb };
