// assets
import { DashboardOutlined, UserOutlined, UserSwitchOutlined, BarsOutlined, BlockOutlined } from '@ant-design/icons';
// icons
const icons = {
    DashboardOutlined,
    UserOutlined,
    UserSwitchOutlined,
    BarsOutlined,
    BlockOutlined
};
// ==============================|| MENU ITEMS - DASHBOARD ||============================== //

const dashboard = {
    id: 'group-dashboard',
    title: 'Navigation',
    type: 'group',
    children: [
        {
            id: 'dashboard',
            title: 'Dashboard',
            type: 'item',
            url: '/dashboard',
            icon: icons.DashboardOutlined,
            breadcrumbs: false
        },
        {
            id: 'user',
            title: 'User',
            type: 'item',
            url: '/user',
            icon: icons.UserOutlined,
            breadcrumbs: false
        },
        {
            id: 'member',
            title: 'Member',
            type: 'item',
            url: '/member',
            icon: icons.UserSwitchOutlined,
            breadcrumbs: false
        },
        {
            id: 'member-receive',
            title: 'MemberReceive',
            type: 'item',
            url: '/member-receive',
            icon: icons.UserSwitchOutlined,
            breadcrumbs: false
        },
        ,
        {
            id: 'category',
            title: 'Category',
            type: 'item',
            url: '/category',
            icon: icons.BarsOutlined,
            breadcrumbs: false
        },
        {
            id: 'package',
            title: 'Package',
            type: 'item',
            url: '/package',
            icon: icons.BlockOutlined,
            breadcrumbs: false
        }
    ]
};

export default dashboard;
