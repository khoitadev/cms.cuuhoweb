import { lazy } from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import useSelectorShallow from 'hooks/useSelectorShallow';
import { getTokenSelector } from 'redux/selectors/userSelector';
// project import
import Loadable from 'components/Loadable';
import MainLayout from 'layout/MainLayout';

// render - dashboard
const Dashboard = Loadable(lazy(() => import('pages/dashboard')));
const User = Loadable(lazy(() => import('pages/user')));
const Member = Loadable(lazy(() => import('pages/member')));
const MemberReceive = Loadable(lazy(() => import('pages/member-receive')));
const Category = Loadable(lazy(() => import('pages/category')));
const Package = Loadable(lazy(() => import('pages/package')));

// render - sample page
const SamplePage = Loadable(lazy(() => import('pages/extra-pages/SamplePage')));

// render - utilities
const Typography = Loadable(lazy(() => import('pages/components-overview/Typography')));
const Color = Loadable(lazy(() => import('pages/components-overview/Color')));
const Shadow = Loadable(lazy(() => import('pages/components-overview/Shadow')));
const AntIcons = Loadable(lazy(() => import('pages/components-overview/AntIcons')));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = () => {
    const token = useSelectorShallow(getTokenSelector);
    console.log('================>>>: token ::: ', token);
    return {
        path: '/',
        element: token ? <Navigate to={'/login'} /> : <MainLayout />,
        children: [
            {
                path: '',
                element: token ? <Navigate to={'/login'} /> : <Dashboard />
            },
            {
                path: 'color',
                element: <Color />
            },
            {
                path: 'dashboard',
                element: <Dashboard />
            },
            {
                path: 'user',
                element: <User />
            },
            {
                path: 'member',
                element: <Member />
            },
            {
                path: 'member-receive',
                element: <MemberReceive />
            },
            {
                path: 'category',
                element: <Category />
            },
            {
                path: 'package',
                element: <Package />
            },
            {
                path: 'sample-page',
                element: <SamplePage />
            },
            {
                path: 'shadow',
                element: <Shadow />
            },
            {
                path: 'typography',
                element: <Typography />
            },
            {
                path: 'icons/ant',
                element: <AntIcons />
            }
        ]
    };
};

export default MainRoutes;
