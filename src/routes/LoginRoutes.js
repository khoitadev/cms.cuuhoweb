import { lazy } from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import useSelectorShallow from 'hooks/useSelectorShallow';
import { getTokenSelector } from 'redux/selectors/userSelector';
// project import
import Loadable from 'components/Loadable';
import MinimalLayout from 'layout/MinimalLayout';

// render - login
const AuthLogin = Loadable(lazy(() => import('pages/authentication/Login')));
const AuthRegister = Loadable(lazy(() => import('pages/authentication/Register')));

// ==============================|| AUTH ROUTING ||============================== //

const LoginRoutes = () => {
    const token = useSelectorShallow(getTokenSelector);
    return {
        path: '/',
        element: token ? <Navigate to={'/login'} /> : <MinimalLayout />,
        children: [
            {
                path: 'login',
                element: token ? <Navigate to={'/dashboard'} /> : <AuthLogin />
            }
            // {
            //     path: 'register',
            //     element: token ? <Navigate to={'/dashboard'} /> : <AuthRegister />
            // }
        ]
    };
};

export default LoginRoutes;
